export interface Task {
    id:string;
    name:string;
    age: string;
    breed:string;
    status:TaskStatus;
}

export enum TaskStatus{
    OPEN = 'OPEN',
    IN_PROGRESS = 'IN_PROGRESS',
    DONE = 'DONE',
}
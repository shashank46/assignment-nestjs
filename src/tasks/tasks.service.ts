import { Injectable } from '@nestjs/common';
import { Task, TaskStatus } from './task.model';
// import * as uuid from 'uuid/';
import {v4 as uuid} from 'uuid'
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { identity } from 'rxjs';
@Injectable()
export class TasksService {
    private tasks: Task[] = []

    getAllTasks(): Task[]{
        return this.tasks;
    }

    getTasksWithFilters(filterDto: GetTasksFilterDto): Task[]
    {
        const {status,search} = filterDto;

        let tasks = this.getAllTasks();

        if(status){
            tasks = tasks.filter(task => task.status===status)
        }

        if(search){
            tasks = tasks.filter(task =>
                task.name.includes(search) ||
                task.age.includes(search) ||
                task.breed.includes(search),
                );
        }

        return tasks;

    }


    getTaskById(id: string): Task {
        return this.tasks.find(task => task.id === id);

    }
    
    
    
    createTask(createTaskDto: CreateTaskDto): Task{
        const{name, age,breed} = createTaskDto;
        
        const task:Task ={
            id: uuid(),
            name,
            age,
            breed,
            status: TaskStatus.OPEN,
            
        };

        this.tasks.push(task);
        return task;
    }

    deleteTask(id: string)
    {
       this.tasks = this.tasks.filter(task => task.id != id);

    }

    updateTaskStatus(id: string,status: TaskStatus,name: string,age:string,breed:string )
    {
        const task =this.getTaskById(id);
        task.status = status;
        task.name=name;
        task.age=age;
        task.breed=breed;
        return task;
    }
}
